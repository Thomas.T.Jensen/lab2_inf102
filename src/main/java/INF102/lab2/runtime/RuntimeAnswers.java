package INF102.lab2.runtime;

public class RuntimeAnswers {

    /*
     * a) O(1)
     * b) O(n^2)
     * c) O(log(n))
     * d) O(nlog(n))
     * e) O(n^2log(n))
     * f) O(n^3)
     * g) O(n)
     */

    // Task A
    public double intrestLoan(double amount, int n) {
        for (int i = 0; i < n; i++) {
            amount = amount * 1.01;
        }
        return amount;
    }

    /*
     * The loop runs n times, and inside the loop, a constant-time operation
     * (multiplication) is performed.
     * Therefore, the time complexity is O(n).
     */

    public String taskA() {
        return "g";
    }

    // TASK B
    public static int countOneBits(int n) {
        int bits = 0;
        while (n > 0) {
            if (n % 2 == 0) {
                bits++;
            }
            n = n / 2;
        }
        return bits;
    }

    /*
     * In the worst case, this algorithm needs to iterate through the number of bits
     * in n,
     * which is proportional to the logarithm of n.
     * Therefore, the time complexity is O(log(n)).
     */
    public String taskB() {
        return "c";
    }

    // TASK C
    public static int countSteps(int n) {
        int pow = 2;
        int steps = 0;
        for (int i = 0; i < n; i++) {
            if (i == pow) {
                pow *= 2;
                for (int j = 0; j < n; j++) {
                    steps++;
                }
            } else {
                steps++;
            }
        }
        return steps;
    }

    /*
     * The outer loop runs n times.
     * The inner loop runs a constant number of times for each power of 2 (1 time
     * for the first, 2 times for the second,
     * 4 times for the third, and so on), which results in a total of O(n log n)
     * operations.
     * Thus, the time complexity is O(n log n).
     * 
     */
    public String taskC() {
        return "d";
    }

    // TASK D
    public static String markRandomString(int n) {
        String ans = "";
        for (int i = 0; i < n; i++) {
            char c = (char) ('a' + 26 * Math.random());
            ans += c;
        }
        return ans;
    }

    /*
     * In each iteration of the loop, a character is appended to the string.
     * The loop runs n times,
     * and the time it takes to concatenate a character to a string is proportional
     * to the current length of the string. Therefore,
     * the overall time complexity is O(n^2)
     */
    public String taskD() {
        return "b";
    }

}
