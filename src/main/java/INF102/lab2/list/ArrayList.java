package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	public static final int DEFAULT_CAPACITY = 10;

	private int n;

	private Object elements[];

	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}

	// Check if the index is within valid range
	private void checkIndex(int index) {
		if (index < 0 || index >= n) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public T get(int index) {

		// Check if the index is within valid range
		checkIndex(index);

		// Return the element at the specified index
		return (T) elements[index];
	}

	// Ensure the capacity of the underlying array
	private void ensureCapacity() {
		int newCapacity = elements.length * 2;
		elements = Arrays.copyOf(elements, newCapacity);
	}

	@Override
	public void add(int index, T element) {
		// Check for valid index range
		if (index < 0 || index > n) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
		// If the array is full, double its capacity
		if (n == elements.length) {
			ensureCapacity();
		}

		// Shift elements to the right to make space for the new element
		for (int i = n; i > index; i--) {
			elements[i] = elements[i - 1];
		}
		// Insert the new element at the specified index
		elements[index] = element;

		// Increment the size of the list
		n++;
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n * 3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n - 1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}