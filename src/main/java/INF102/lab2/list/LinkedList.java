package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;

	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	// Helper method to add an element at the beginning of the list
	public void addFirst(T element) {
		Node<T> newNode = new Node<>(element);
		newNode.next = head;
		head = newNode;
		n++;
	}

	// Helper method to add an element at the end of the list
	public void addLast(T element) {
		Node<T> newNode = new Node<>(element);
		if (isEmpty()) {
			head = newNode;
		} else {
			Node<T> lastNode = getNode(n - 1);
			lastNode.next = newNode;
		}
		n++;
	}

	private void checkIndex(int index) {
		if (index < 0 || index >= n) {
			throw new IndexOutOfBoundsException("Index out of range");
		}
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}

	/**
	 * Returns the node at the specified position in this list.
	 *
	 * @param index index of the node to return
	 * @return the node at the specified position in this list
	 * @throws IndexOutOfBoundsException if the index is out of range
	 *                                   ({@code index < 0 || index >= size()})
	 */

	// Returns the node at the specified position in this list.
	private Node<T> getNode(int index) {
		checkIndex(index);
		Node<T> currentNode = head;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode;
	}

	@Override
	public void add(int index, T element) {
		// Check for valid index range
		if (index < 0 || index > n) {
			throw new IndexOutOfBoundsException("Index out of range");
		}

		// Adding at the beginning
		if (index == 0) {
			addFirst(element);

			// Adding at the end.
		} else if (index == n) {
			addLast(element);

			// Adding at a specific index.
		} else {
			Node<T> prevNode = getNode(index - 1);
			Node<T> newNode = new Node<>(element);
			newNode.next = prevNode.next;
			prevNode.next = newNode;
			n++;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n * 3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}

}

/*
 * The two helper methods, addFirst and addLast, are necessary to provide a more
 * efficient and comprehensive way of adding elements to the linked list.
 * 
 * By providing these helper methods, you encapsulate the logic for adding
 * elements at the beginning and end of the list, making the main add method
 * cleaner and easier to understand. Additionally, these helper methods improve
 * the efficiency of adding elements at the head and tail of the list, ensuring
 * that the linked list remains a viable data structure for various scenarios.
 */